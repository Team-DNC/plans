Plans Telzir
==============
A empresa de telefonia Telzir, especializada em chamadas de longa distância nacional, vai
colocar um novo produto no mercado chamado FaleMais.
Com o novo produto FaleMais da Telzir o cliente adquire um plano e pode falar de graça até
um determinado tempo (em minutos) e só paga os minutos excedentes. Os minutos
excedentes tem um acréscimo de 10% sobre a tarifa normal do minuto. Os planos são
FaleMais 30 (30 minutos), FaleMais 60 (60 minutos) e FaleMais 120 (120 minutos).

### Arquitetura e Tecnologias
O projeto foi desenvolvido em ASP.Net Core MVC utilizando Visual Studio 2017.

### Executando a solução
Para executar, localize o arquivo: "Telzir.Plans.sln" na pasta do Projeto Plans e abra-o no Visual Studio 2017. 

### Testando a aplicação.
Para iniciar os testes clique em: "[menu ]Test > Windows > Test Explorer" e em seguida clique em "Run All", caso não ocorram erro, será apresentado informação de sucesso no teste.

### Executando a aplicação.

1. Localize o projeto "Telzir.Plans.Web"
2. Certifique-se que está é o projeto inicial da solução
3. Clique em IIS Express, o site será iniciado e uma tela de browser será aberta

### Utilizando a aplicação

1. Escolha um DDD de Origem
2. Escolha um DDD de Destino
3. Escolha um dos planos disponíveis
4. Informe um numeros em minutos para iniciar as simulações