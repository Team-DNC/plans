﻿using NUnit.Framework;
using Telzir.Plans.Web.Business;
using Telzir.Plans.Web.Models;

namespace Telzir.Plans.Test
{
    [TestFixture]
    public class ServiceTest
    {
        private readonly Simulate _simulateServiceTest;
        private readonly PlansRate _plansRateServiceTest;

        public ServiceTest()
        {
            _simulateServiceTest = new Simulate();
            _plansRateServiceTest = new PlansRate();
        }

        [Test]
        public void SimulateToFromTimePlanWithPlan()
        {
            _simulateServiceTest.SimulateDDD("011", "016", 30, 1);
            Assert.AreEqual(0,_simulateServiceTest.PlanSimulate.WithPlan);
        }

        [Test]
        public void SimulateToFromTimePlanWithoutPlan()
        {
            _simulateServiceTest.SimulateDDD("011", "016", 30, 1);
            Assert.AreEqual(87, _simulateServiceTest.PlanSimulate.WithoutPlan);
        }

        [Test]
        public void CheckObjectPlanNameIsNotNull()
        {
            _simulateServiceTest.SimulateDDD("011", "016", 30, 1);
            Assert.IsNotNull(_simulateServiceTest.PlanName);
        }

        [Test]
        public void CheckObjectPlanRateIsNotNull()
        {
            _simulateServiceTest.SimulateDDD("011", "016", 30, 1);
            Assert.IsNotNull(_simulateServiceTest.PlanRate);
        }

        [Test]
        public void CheckExistsFile()
        {
            _plansRateServiceTest.GetListPlanRate();
            Assert.IsNotNull(_plansRateServiceTest);
        }
    }
}