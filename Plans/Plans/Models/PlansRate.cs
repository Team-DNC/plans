﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Telzir.Plans.Web.Models
{
    public class PlansRate
    {
        public string To { get; set; }
        public string From { get; set; }
        public double PriceForMinute { get; set; }
        
        public List<PlansRate> GetListPlanRate()
        {
            using (StreamReader r = new StreamReader(Path.GetFullPath("~/wwwroot/database/plansRate.json").Replace("~\\", "").ToString()))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<PlansRate>>(json);
            }
        }
    }
}