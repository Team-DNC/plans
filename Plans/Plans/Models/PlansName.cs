﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Telzir.Plans.Web.Models
{
    public class PlansName
    {
        public int IDPlanName { get; set; }
        public string Name { get; set; }
        public int FreeTime { get; set; }
        public double Taxa { get; set; }
      
        public List<PlansName> GetListPlansName()
        {
            using (StreamReader r = new StreamReader(Path.GetFullPath("~/wwwroot/database/plansName.json").Replace("~\\", "").ToString()))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<PlansName>>(json);
            }
        }
    }
}