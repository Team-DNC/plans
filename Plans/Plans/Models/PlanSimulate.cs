﻿
namespace Telzir.Plans.Web.Models
{
    public class PlanSimulate
    {
        public string To { get; set; }
        public string From { get; set; }
        public double Time { get; set; }
        public double WithPlan { get; set; }
        public double WithoutPlan { get; set; }
        public int IDPlanName { get; set; }
        public double Taxa { get; set; }
    }
}