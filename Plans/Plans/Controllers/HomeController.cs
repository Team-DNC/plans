﻿using Microsoft.AspNetCore.Mvc;
using Telzir.Plans.Web.Business;
using Telzir.Plans.Web.Models;

namespace Telzir.Plans.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Telzir sempre oferecendo o melhor para seu cliente.";

            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(PlanSimulate param)
        {
            var oSimulate = new Simulate();
            oSimulate.SimulateDDD(param.To, param.From, param.Time, param.IDPlanName);
            return Json(oSimulate);
        }
    }
}