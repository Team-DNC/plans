﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function setFrom() {
    var options = [];
    options.push('<option value="011">DDD 011</option>');
    options.push('<option value="016">DDD 016</option>');
    options.push('<option value="017">DDD 017</option>');
    options.push('<option value="018">DDD 018</option>');
    $("#From").html(options.join(''));
    $("#From option[value='" + $('#To').val() + "']").remove();
}

function simulate() {
    $(".btnSimular").text("Aguarde...");
    $.post("/Home/Index", $('form').serialize())
        .done(function (data) {
            if (data) {
                if (data.planSimulate != null) {
                    $(".WithPlan").text("Com FaleMais R$:  " + float2moeda(data.planSimulate.withPlan) + "");
                    $(".WithoutPlan").text("Sem FaleMais R$:  " + float2moeda(data.planSimulate.withoutPlan) + "");
                    $(".Taxa").text("Taxa de Adesão R$ " + float2moeda(data.planSimulate.taxa) + "");
                    $(".btnSimular").text("Simular");
                    $('.alert').hide()
                }
                else {
                    $(".WithPlan").text("");
                    $(".WithoutPlan").text("");
                    $(".Taxa").text("");
                    $(".btnSimular").text("Simular");
                    $('.alert').show()
                }
            }
        })
}

function float2moeda(num) {

    x = 0;

    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num)) num = "0";
    cents = Math.floor((num * 100 + 0.5) % 100);

    num = Math.floor((num * 100 + 0.5) / 100).toString();

    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
            + num.substring(num.length - (4 * i + 3));
    ret = num + ',' + cents;
    if (x == 1) ret = ' - ' + ret; return ret;

}