﻿using System.Linq;
using Telzir.Plans.Web.Models;

namespace Telzir.Plans.Web.Business
{
    public class Simulate
    {
        public PlansName PlanName { get; set; }
        public PlansRate PlanRate { get; set; }
        public PlanSimulate PlanSimulate { get; set; }

        public void SimulateDDD(string To, string From, double Time, int IDPlanName)
        {
            try
            {
                PlanName = new PlansName().GetListPlansName().Where(p => p.IDPlanName.Equals(IDPlanName)).FirstOrDefault();
                PlanRate = new PlansRate().GetListPlanRate().Where(p => p.To.Equals(To) && p.From.Equals(From)).FirstOrDefault();

                if (PlanName != null && PlanRate != null)
                    PlanSimulate = new PlanSimulate
                    {
                        To = To,
                        From = From,
                        Time = Time,
                        WithPlan = Time > PlanName.FreeTime ? (((Time - PlanName.FreeTime) * PlanRate.PriceForMinute) + (0.1 * ((Time - PlanName.FreeTime) * PlanRate.PriceForMinute))) : 0,
                        WithoutPlan = PlanRate.PriceForMinute * Time,
                        Taxa = PlanName.Taxa
                    };
            }
            catch
            {
                throw;
            }
        }
    }
}